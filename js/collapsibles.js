function collapse(element) {
  var _element = document.getElementsByClassName(element);
  if (_element[0].className == element + " collapsed") {
    _element[0].className = element + " uncollapsed";
  } else {
    _element[0].className = element + " collapsed";
  }
}
